/*######################
ROS Node Built on top of CommSignia V2X SDK to publish CPM with detected object from individual sensors from IN2Lab Project
Dependencies: 1. CommSignia SDK
              2. ETSI ROS Messages
Author: Shrivathsa Udupa
Email:  shrivatsa.udupa@carissma.eu
IN2Lab Project:
ROS Noetic on Ubuntu 20.04
#####################*/
#include "ros/ros.h"
#include <sstream>
#include <v2x/v2x.h>
#include <v2x/v2x-fac.h>
#include <stdio.h>
#include <unistd.h>
#include <in2lab_ros_msg/TrackObject.h>
#include <arpa/inet.h>
#include <vector>
#include<commsignia_node.h>






using namespace commsignia_node;

int main(int argc, char **argv)
{   
    ros::init(argc,argv,"CommSignia_ROS_CPM");
    ros::NodeHandle nh;
    ROS_INFO("CommSignia_ROS_CPM Started");
    static v2x_s_session_t  l_s_its = V2X_S_SESSION_INITIALIZER;
    std::cout<<"Host Name is "<<argv[1]<<std::endl;
    PublishCPM v2x_publisher=PublishCPM(&nh,argv,l_s_its);

    int hz,reset_interval,loop_counter;
    nh.getParam("rate",hz);
    nh.getParam("reset_interval",reset_interval);

    ros::Rate loop_rate(hz);

    while (ros::ok())
    {
        loop_counter++;
        v2x_publisher.cpm_publisher(v2x_publisher.l_s_its_,&v2x_publisher.pos,v2x_publisher.s_cpm,v2x_publisher.s_send_data_shb);
        v2x_publisher.ros_cam_publisher();
        v2x_publisher.ros_cpm_publisher();
        ros::spinOnce();
        loop_rate.sleep();
        if(loop_counter%hz==0)
            {
                v2x_publisher.publish_sensors(PublishCPM::s_cpm);
                std::cout<<"Publishing Sensors\n";

            }
        //Clear All Published Objects every 10 Second
        if(hz*reset_interval==loop_counter)
        {
            v2x_publisher.published_objects.clear();
            loop_counter=0;
        }

    }
}