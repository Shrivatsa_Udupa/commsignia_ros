#ifndef COMMSIGNIA_NODE_H_
#define COMMSIGNIA_NODE_H_

#include <sstream>
#include <v2x/v2x.h>
#include <v2x/v2x-fac.h>
#include <stdio.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <vector>
#include <etsi_its_msgs/CPM.h>
#include <etsi_its_msgs/CAM.h>
#include <in2lab_ros_msg/TrackObjectList.h>
#include <in2lab_ros_msg/TrackObject.h>
#include "ros/ros.h"


namespace commsignia_node{


/* ITS AID of the example application */
#define L_U64_ITS_AID                       28ULL
#define L_U32_ITSPDU_LEN                    6U
#define L_U64_TAI_OFFSET_BUG_MS            (37U * 1000U)

/*Destination Port for CPM Publishing*/
#define L_U16_DEST_PORT                     2009U
#define L_S16_TX_POWER                      0
#define L_E_IF_ID                           V2X_E_IFINDEX_1

class PublishCPM {

    public:

        //Commsignia API Required Variables
        v2x_e_err_t e_err = V2X_E_ERR_GENERAL;
        v2x_s_callbacks_t s_callbacks = V2X_S_CALLBACKS_INITIALIZER;
        uint32_t u32_fac_subscr_type = 0U;
        static v2x_s_pos_t pos;
        static v2x_s_cpm_t s_cpm;
        v2x_s_gn_send_common_t s_send_data_shb = V2X_S_GN_SEND_COMMON_INITIALIZER;
        uint64_t u64_station_id = 0ULL;
        uint16_t u16_data_len = 0U;
        uint16_t u16_event_flags = 0U;
        v2x_s_session_t  l_s_its_ = V2X_S_SESSION_INITIALIZER;


        //Object List Variables
        std::vector<in2lab_ros_msg::TrackObject> camera_right_objects_list;
        std::vector<in2lab_ros_msg::TrackObject> camera_left_objects_list;
        std::vector<in2lab_ros_msg::TrackObject> camera_fisheye_objects_list;
        std::vector<in2lab_ros_msg::TrackObject> radar_right_objects_list;
        std::vector<in2lab_ros_msg::TrackObject> radar_left_objects_list;
        std::vector<in2lab_ros_msg::TrackObject> lidar_right_objects_list;
        std::vector<in2lab_ros_msg::TrackObject> lidar_left_objects_list;

        std::vector<in2lab_ros_msg::TrackObject> published_objects;
        std::vector<in2lab_ros_msg::TrackObject> objects_to_publish;

        static etsi_its_msgs::CAM CAM_message;
        static etsi_its_msgs::CPM CPM_message;
        

        //Sensor IDs
        enum sensor_id
        {
            RADAR_RIGHT=0,
            RADAR_LEFT=1,
            LIDAR_RIGHT=2,
            LIDAR_LEFT=3,
            CAMERA_RIGHT=4,
            CAMERA_LEFT=5,
            CAMERA_FISHEYE=6

        };
    


    //Flag Variables
        static bool camera_right_has_objects;
        static bool camera_left_has_objects;
        static bool camera_fisheye_has_objects;
        static bool radar_right_has_objects;
        static bool radar_left_has_objects;
        static bool lidar_right_has_objects;
        static bool lidar_left_has_objects;
                    
        static bool new_message_cam;
        static bool new_message_cpm;
        
         
        int no_of_objects=0;
        static int no_of_sensor;


  


         //Matrices
        int total_objects_from_sensor;
        int total_object_published_in_cpm;  


        //ROS Subscribers
        ros::Subscriber Camera_right_subscriber;
        ros::Subscriber Lidar_right_subscriber; 
        ros::Subscriber Radar_right_subscriber;
        ros::Subscriber Camera_left_subscriber;
        ros::Subscriber Lidar_left_subscriber ; 
        ros::Subscriber Radar_left_subscriber; 
        ros::Subscriber Camera_fisheye_subscriber;
        ros::Subscriber c2x_car_object;

        //ROS Publisher
        ros::Publisher pub_cam;
        ros::Publisher pub_cpm;

        std::string camera_right_topic,camera_left_topic,radar_right_topic,radar_left_topic,camera_fisheye_topic,lidar_left_topic,lidar_right_topic;

        std::string cam_Publish_topic,cpm_Publish_topic;

        
    


        
        //Constructer
        PublishCPM(ros::NodeHandle *nh,char **argv,v2x_s_session_t l_s_its);

        //Destrcuter
    

        //Initializer meathod
        void onInit();

        //Method to add sensor to CPM Container
        v2x_s_cpm_t l_add_sensor(uint8_t p_u8_sensor_id,uint8_t p_u8_sensor_type,v2x_s_cpm_t s_cpm);

        //Callback method to hold Navigation Data
        static void l_nav_notif_callback(const v2x_s_pos_t* p_sp_pos, void* p_vp_ctx);
        
        //Method to implement CPM filter methods
        void publish_sensors(v2x_s_cpm_t s_cpm);

        //Facility notification callback method: Capture the received CAM and CPM into ROS Topics
        static void l_fac_notif_callback(const v2x_s_fac_notif_t* p_sp_fac_notif,
                        const uint8_t* p_u8p_data,
                        uint32_t p_u32_length, void* p_vp_ctx);


        //Method to publish the ROS topic with current received CAM values
        void ros_cam_publisher();
            

        //Method to publish the ROS topic with current received CPM values
        void ros_cpm_publisher();

        static void capture_cam_values(const uint8_t* p_u8p_buf, uint16_t u16_length);

        //Method to capture the Received CPM Value before publishing them as ROS topics
        static void capture_cpm_values(const uint8_t* p_u8p_buf, uint16_t p_u16_len);


        //Method to clear the data from CPM Message after publishing
        //void clear();

        //Method to get index of the object in the vector based on tracking ID
        int getIndex(std::vector<in2lab_ros_msg::TrackObject > vals,in2lab_ros_msg::TrackObject x);


        bool is_static(std::vector<in2lab_ros_msg::TrackObject> pub_objects,in2lab_ros_msg::TrackObject object);
        //Method to check if Object is new
        bool is_new(in2lab_ros_msg::TrackObject object);
        bool is_moving(in2lab_ros_msg::TrackObject object);


        v2x_s_cpm_t add_objects_to_cpm(std::vector<in2lab_ros_msg::TrackObject> objects_to_publish,v2x_s_cpm_t s_cpm);
        /*
        Method to add Recent Detected objects from Sensors to CPM container from ROS topic.
        */

        void cpm_publisher(v2x_s_session_t l_s_its ,v2x_s_pos_t *position, v2x_s_cpm_t s_cpm,v2x_s_gn_send_common_t s_send_data_shb);

        //Callback method to capture the objects from right camera sensor ros topic
        void callBack_camera_right(const in2lab_ros_msg::TrackObjectList& msg);

        //Callback method to capture the objects from left camera sensors ros topic
        void callBack_camera_left(const in2lab_ros_msg::TrackObjectList& msg);

        //Callback method to capture the objects detected from Fisheye camera ROS topic
        void callBack_camera_fisheye(const in2lab_ros_msg::TrackObjectList& msg);

        //Callback method to capture the objects from the right LIDAR sensor ros topic
        void callBack_lidar_right(const in2lab_ros_msg::TrackObjectList& msg);

        //Callback method to capture the objects from the left LIDAR sensor ros topic
        void callBack_lidar_left(const in2lab_ros_msg::TrackObjectList& msg);

        //Callback method to capture the objects from the right radar sensor ROS topic
        void callBack_radar_right(const in2lab_ros_msg::TrackObjectList& msg);

        //Callback method to capture the objects from the left radar sensor ROS topic
        void callBack_radar_left(const in2lab_ros_msg::TrackObjectList& msg);


                
            


};

}


#endif 