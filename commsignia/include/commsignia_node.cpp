#include <commsignia_node.h>
#include "std_msgs/String.h"
#include "std_msgs/Time.h"
#include <etsi_its_msgs/CPM.h>
#include <etsi_its_msgs/CAM.h>
#include <in2lab_ros_msg/TrackObjectList.h>
#include <in2lab_ros_msg/TrackObject.h>


//using namespace std;
using namespace in2lab_ros_msg;

namespace commsignia_node{



void PublishCPM::onInit()
{

    //Configure Single Hop Broadcasting
    s_send_data_shb.e_interface = L_E_IF_ID;
    s_send_data_shb.s_tr_class.e_scf = V2X_E_GN_SCF_DISABLED;
    s_send_data_shb.s_tr_class.e_ch_offl = V2X_E_GN_CH_OFFL_DISABLED;
    s_send_data_shb.s_tr_class.u8_id = 0U;
    s_send_data_shb.u16_sport_dinfo = V2X_E_BTP_A;
    s_send_data_shb.u16_dport = L_U16_DEST_PORT;
    s_send_data_shb.e_btp = V2X_E_BTP_B;
    /* Set Tx power */
    s_send_data_shb.s16_tx_power = L_S16_TX_POWER;
    /* Set Security Profile */
    s_send_data_shb.b_security_sign = 0;

    PublishCPM::s_callbacks.fp_fac_notif = l_fac_notif_callback;
    PublishCPM::s_callbacks.fp_nav_notif = l_nav_notif_callback;
    PublishCPM::u32_fac_subscr_type = V2X_U32_FAC_SUBS_TYPE_CPM | V2X_U32_FAC_SUBS_TYPE_CAM;



}


PublishCPM::PublishCPM(ros::NodeHandle *nh,char **argv,v2x_s_session_t l_s_its)
{

    
    nh->getParam("lidar_right_topic",lidar_right_topic);
    nh->getParam("lidar_left_topic",lidar_left_topic);
    nh->getParam("radar_right_topic",radar_right_topic);
    nh->getParam("radar_left_topic",radar_left_topic);
    nh->getParam("camera_left_topic",camera_left_topic);
    nh->getParam("camera_right_topic",camera_right_topic);
    nh->getParam("camera_fisheye_topic",camera_fisheye_topic);


    nh->getParam("cam_publisher_ros_topic",cam_Publish_topic);
    nh->getParam("cpm_publisher_ros_topic",cpm_Publish_topic);

    
    std::cout<<camera_right_topic;




        //Initialize Commsignia API
    e_err = v2x_api_init();
    if(v2x_error(PublishCPM::e_err))
        {
            fprintf(stderr, "Unable to initialize Unplugged ITS API %s\n",v2x_get_err_str(e_err));
            exit(0);
        } 
    else
        {
            printf("Unplugged ITS API is initialized\n");
        }
                

    PublishCPM::e_err = v2x_register_aid(&l_s_its, L_U64_ITS_AID, &s_callbacks, NULL);

    if(v2x_error(PublishCPM::e_err))
        {
        fprintf(stderr, "Unable to register ITS-AID %s\n",v2x_get_err_str(e_err));
        exit(0);
        //return -1;
        } 
    else 
        {
            printf("ITS_AID registered\n");
        }

    PublishCPM::e_err = v2x_remote_api_connect(&l_s_its, argv[1]);
    if(v2x_error(PublishCPM::e_err))
    {
        fprintf(stderr, "Unable to connect to the device %s\n",v2x_get_err_str(e_err));
        exit(0);
    } 
    else{
        printf("Connected.\n");
    }

    //Facility subscription for CAM CPM Message
    

    /* Subscribe to Facility notifications */
    PublishCPM::e_err = v2x_fac_subscribe(&l_s_its, u32_fac_subscr_type);
    if(v2x_error(PublishCPM::e_err)){
        fprintf(stderr, "Unable to subscribe to Facility notifications %s\n", v2x_get_err_str(e_err));
        exit(0);
    } 
    else {
        printf("Facility subscription is OK\n");
    }

    PublishCPM::e_err=v2x_poti_nav_subscribe(&l_s_its);
    if(v2x_error(PublishCPM::e_err))
    {
        fprintf(stderr, "Unable to subscribe to Navigation notifications %s\n", v2x_get_err_str(e_err));
        exit(0);
    } 
    else 
    {
        printf("Navigation subscription is OK\n");
    }
    l_s_its_=l_s_its;
    //Add Sensors to CPM for first message
    PublishCPM::no_of_sensor=0;  
    publish_sensors(PublishCPM::s_cpm);



    onInit();

    Camera_right_subscriber=nh->subscribe(camera_right_topic, 1000, &PublishCPM::callBack_camera_right,this);
    Lidar_right_subscriber=nh->subscribe(lidar_right_topic, 1000, &PublishCPM::callBack_lidar_right,this);
    //Radar_right_subscriber=nh->subscribe("/Radar_right_Objects", 1000, &PublishCPM::callBack_radar_right,this);
    Radar_right_subscriber=nh->subscribe(radar_right_topic, 1000, &PublishCPM::callBack_radar_right,this);
    Camera_left_subscriber=nh->subscribe(camera_left_topic, 1000, &PublishCPM::callBack_camera_left,this);
    Lidar_left_subscriber=nh->subscribe(lidar_left_topic, 1000, &PublishCPM::callBack_lidar_left,this);
    Radar_left_subscriber=nh->subscribe(radar_left_topic, 1000, &PublishCPM::callBack_radar_left,this);
    Camera_fisheye_subscriber=nh->subscribe(camera_fisheye_topic, 1000, &PublishCPM::callBack_camera_fisheye,this);

    pub_cam=nh->advertise<etsi_its_msgs::CAM>(cam_Publish_topic,100);
    pub_cpm=nh->advertise<etsi_its_msgs::CPM>(cpm_Publish_topic,100); 

    std::cout<<"Initialization Success\n";

}

v2x_s_cpm_t PublishCPM::l_add_sensor(uint8_t p_u8_sensor_id,uint8_t p_u8_sensor_type,v2x_s_cpm_t s_cpm)
    {
        s_cpm.b_sensor_info=1;
        s_cpm.as_sensor_infos[no_of_sensor].u8_identifier=p_u8_sensor_id;

        s_cpm.as_sensor_infos[no_of_sensor].u8_sensor_type=p_u8_sensor_type;

        //To-Do: Adding Sensor Region Info   
        s_cpm.as_sensor_infos[no_of_sensor].b_occupied_present=0;
        s_cpm.as_sensor_infos[no_of_sensor].b_occupied=0;

        s_cpm.as_sensor_infos[no_of_sensor].s_detection_area.e_detection_area = V2X_E_CPM_DETECTION_AREA_STAT_SENSOR_RADIAL;
        no_of_sensor++;
        s_cpm.u32_sensor_info_length=no_of_sensor;  

        //To-Do: Add Other sensor parameters
        return s_cpm;              
    }
void PublishCPM::l_nav_notif_callback(const v2x_s_pos_t* p_sp_pos, void* p_vp_ctx)

    {
        PublishCPM::pos=*p_sp_pos;
    }

void PublishCPM::publish_sensors(v2x_s_cpm_t s_cpm)
    {
        //Add sensor information container
        no_of_sensor=0;
        s_cpm=l_add_sensor(CAMERA_RIGHT,V2X_E_CPM_SENSOR_TYPE_MONO_VIDEO,PublishCPM::s_cpm);
        s_cpm=l_add_sensor(CAMERA_LEFT,V2X_E_CPM_SENSOR_TYPE_MONO_VIDEO,PublishCPM::s_cpm);
        s_cpm=l_add_sensor(CAMERA_FISHEYE,V2X_E_CPM_SENSOR_TYPE_MONO_VIDEO,PublishCPM::s_cpm);
        s_cpm=l_add_sensor(LIDAR_RIGHT,V2X_E_CPM_SENSOR_TYPE_LIDAR,PublishCPM::s_cpm);
        s_cpm=l_add_sensor(LIDAR_LEFT,V2X_E_CPM_SENSOR_TYPE_LIDAR,PublishCPM::s_cpm);
        s_cpm=l_add_sensor(RADAR_RIGHT,V2X_E_CPM_SENSOR_TYPE_RADAR,PublishCPM::s_cpm);
        s_cpm=l_add_sensor(RADAR_LEFT,V2X_E_CPM_SENSOR_TYPE_RADAR,PublishCPM::s_cpm);

        //Clear published objects vector
        if(published_objects.size()>0)
        {
            published_objects.clear();
        }

    }
void PublishCPM::l_fac_notif_callback(const v2x_s_fac_notif_t* p_sp_fac_notif,
                const uint8_t* p_u8p_data,
                uint32_t p_u32_length, void* p_vp_ctx)
    {
        
        (void)p_vp_ctx;
        uint16_t u16_length = 0U;

        // PublishCV2X temp(nh,argv,l_s_its);
        

        if((NULL == p_sp_fac_notif) || (NULL == p_u8p_data)) {
            fprintf(stderr, "%s NULL argument\n", __func__);
            return;
        }  

        switch(p_sp_fac_notif->e_type) {
            case V2X_E_FAC_CAM:                                                               
                        u16_length = (uint16_t)p_u32_length;
                        capture_cam_values(p_u8p_data,  u16_length);
                        break;
                        
            
            case V2X_E_FAC_CPM:   
                        capture_cpm_values(p_u8p_data + L_U32_ITSPDU_LEN, p_u32_length - L_U32_ITSPDU_LEN);
                        break;

            }

        }
void PublishCPM::ros_cam_publisher()
    {
        if(PublishCPM::new_message_cam)
                {
                ROS_INFO("CAM Published");
                pub_cam.publish(PublishCPM::CAM_message);
                PublishCPM::new_message_cam=false;
                }
        
    }
void PublishCPM::ros_cpm_publisher()
    {
        if(PublishCPM::new_message_cpm)
                {
                ROS_INFO("CPM Published");
                pub_cpm.publish(PublishCPM::CPM_message);
                PublishCPM::new_message_cpm=false;
                PublishCPM::CPM_message={};
                }
    }

void PublishCPM::capture_cam_values(const uint8_t* p_u8p_buf, uint16_t u16_length)
{          
        v2x_e_err_t e_err = V2X_E_ERR_GENERAL;
        v2x_s_cam_t s_cam = V2X_S_CAM_INITIALIZER;
        v2x_s_pdu_header_t s_pdu,s_cpm_pdu = V2X_S_PDU_INITIALIZER;
        e_err = v2x_decode_cam_with_itspdu(p_u8p_buf, u16_length,&s_cam,&s_pdu);
        if(v2x_error(e_err))
                        {
                        fprintf(stderr, "Cannot decode CAM! %s\n", v2x_get_err_str(e_err));
                        PublishCPM::new_message_cam=false; 
                        return;
                        }
        else
        {   
            PublishCPM::new_message_cam=true; 
            PublishCPM::CAM_message.its_header.station_id=s_pdu.u32_station_id; 
            PublishCPM::CAM_message.its_header.protocol_version=s_pdu.u8_protocol_version;
            PublishCPM::CAM_message.its_header.message_id=s_pdu.e_msg_id;
            PublishCPM::CAM_message.station_type.value=(u_int8_t)s_cam.e_station_type;
            PublishCPM::CAM_message.generation_delta_time=s_cam.u16_generation_delta_time;
            PublishCPM::CAM_message.has_low_frequency_container=s_cam.b_low_frequency_cont;
            
            //TODO: Assign other Values of CAM Message
            //PublishCV2X::CAM_message.high_frequency_container=s_cam.u_cam_high_freq_cont.s_basic_vehicle;
            
            PublishCPM::CAM_message.reference_position.latitude=s_cam.s32_latitude;
            PublishCPM::CAM_message.reference_position.longitude=s_cam.s32_longitude;
            PublishCPM::CAM_message.reference_position.altitude.value=s_cam.s32_altitude_value;
            return;
        }

}


void PublishCPM::capture_cpm_values(const uint8_t* p_u8p_buf, uint16_t p_u16_len)
    {
        v2x_s_cpm_t s_cpm_received;
        int counter;
        int numOfObjects;
        v2x_e_err_t e_err = V2X_E_ERR_GENERAL;
        etsi_its_msgs::PerceivedObject percievedObject;
        e_err=v2x_decode_cpm(p_u8p_buf, p_u16_len, &s_cpm_received);
        if(v2x_error(e_err))
        {
        fprintf(stderr, "Cannot decode CPM! %s\n", v2x_get_err_str(e_err));
        return;
        }

        else
        {
            PublishCPM::new_message_cpm=true;
            counter=0;


            //Header Values
            //PublishCV2X::CPM_message.its_header.station_id=s_cpm_pdu.u32_station_id;
            //PublishCV2X::CPM_message.its_header.message_id=s_cpm_pdu.e_msg_id;
            //PublishCV2X::CPM_message.its_header.protocol_version=s_cpm_pdu.u8_protocol_version;

            //CPM Parameter container
            PublishCPM::CPM_message.generation_delta_time=s_cpm_received.u16_generation_delta_time;

            //CPM Management Container
            PublishCPM::CPM_message.station_type.value=s_cpm_received.e_station_type;
            PublishCPM::CPM_message.reference_position.latitude=s_cpm_received.s_ref_pos.s32_latitude;
            PublishCPM::CPM_message.reference_position.longitude=s_cpm_received.s_ref_pos.s32_longitude;
            PublishCPM::CPM_message.reference_position.altitude.value=s_cpm_received.s_ref_pos.s32_altitude_value;

            //CPM Originating Vehicle Container
            //TODO: Add other relavent info
            PublishCPM::CPM_message.originatingVehicleContainer.heading.value=s_cpm_received.s_station_data.u_station_data.s_vehicle.u16_heading_value;
            PublishCPM::CPM_message.originatingVehicleContainer.speed.value=s_cpm_received.s_station_data.u_station_data.s_vehicle.u16_spd_value;
            PublishCPM::CPM_message.originatingVehicleContainer.drive_direction.value=s_cpm_received.s_station_data.u_station_data.s_vehicle.e_drive_direction;                                            
            //Sensor Info
            PublishCPM::CPM_message.has_sensor_information_container=s_cpm_received.b_sensor_info;
            if(s_cpm_received.b_sensor_info)
            {
                //TODO: Add Sensor information details
            }

            PublishCPM::CPM_message.has_list_of_perceived_object=true;
            
            
            PublishCPM::CPM_message.numberOfPerceivedObjects=s_cpm_received.u32_perieved_obj_length;
            numOfObjects=s_cpm_received.u32_perieved_obj_length;
                
            //std::cout<<"Counter= "<<counter<<" Number of objects= "<<numOfObjects<<std::endl;
            
            //PublishCV2X::CPM_message.listOfPerceivedObjects.perceivedObjectContainer[0].objectID=s_cpm.as_perceived_objs[0].u8_obj_id;
            for(counter=0;counter<numOfObjects;counter++)
            {
                //TODO: Add other Relavent containers
                percievedObject.objectID=s_cpm_received.as_perceived_objs[counter].u8_obj_id;  
                percievedObject.sensorID=s_cpm_received.as_perceived_objs[counter].u8_sensor_id;
                percievedObject.timeOfMeasurement=s_cpm_received.as_perceived_objs[counter].s16_time_of_measurement;
                percievedObject.xDistance.value=s_cpm_received.as_perceived_objs[counter].s32_x_dist_value;
                percievedObject.yDistance.value=s_cpm_received.as_perceived_objs[counter].s32_y_dist_value;
                percievedObject.zDistance.value=s_cpm_received.as_perceived_objs[counter].s32_z_dist_value;
                percievedObject.xSpeed.value=s_cpm_received.as_perceived_objs[counter].s16_x_spd_value;
                percievedObject.ySpeed.value=s_cpm_received.as_perceived_objs[counter].s16_y_spd_value;
                percievedObject.zSpeed.value=s_cpm_received.as_perceived_objs[counter].s16_z_spd_value;
                percievedObject.classification.value=s_cpm_received.as_perceived_objs[counter].b_classification;
                PublishCPM::CPM_message.listOfPerceivedObjects.perceivedObjectContainer.push_back(percievedObject);

            } 


        }
    }
int PublishCPM::getIndex(std::vector<in2lab_ros_msg::TrackObject > vals,in2lab_ros_msg::TrackObject x)
    {
        auto it = find_if(vals.rbegin(), vals.rend(), [&x](const in2lab_ros_msg::TrackObject& obj) {return obj.tracking_id == x.tracking_id;});

        
        if (it != vals.rend())
        {
        // found element. it is an iterator to the first matching element.
        // if you really need the index, you can also get it:
        auto index = std::distance(vals.rbegin(), it);
        return index;
        }
        
        else return -1;
        
    }

bool PublishCPM::is_static(std::vector<in2lab_ros_msg::TrackObject> pub_objects,in2lab_ros_msg::TrackObject object)
    {
        if(!pub_objects.empty())
            {  
                int object_index=getIndex(pub_objects,object);
                if(object_index>-1)
                {  
                    //Check last published time stamp for the object
                    if((pub_objects[object_index].timestamp-object.timestamp).toSec()>1)
                    {
                        return true;
                    }
                    
                    
                }
                else
                {
                    return false;
                    
                }

            }
            else
            {
                return false;
            }

            return false;
    }

bool PublishCPM::is_new(in2lab_ros_msg::TrackObject object)
    {
            if(!published_objects.empty())
            {
                if(getIndex(published_objects,object)>-1)
                {
                    return false;
                }
                else
                {
                    return true;
                }

            }
            else
            {
                return true;
            }
    }

bool PublishCPM::is_moving(in2lab_ros_msg::TrackObject object)
    {
        if(!published_objects.empty())
            {  
                int object_index=getIndex(published_objects,object);
                if(object_index>-1)
                {  
                    //Check if Object has moved more than 4m or velocity changed more than 0.4m/s
                    if((abs(object.obj_pose_x - published_objects[object_index].obj_pose_x)>4) || 
                    (abs(sqrt((object.vx*object.vx)+(object.vy*object.vy)) - sqrt((published_objects[object_index].vx*published_objects[object_index].vx)+(published_objects[object_index].vy*published_objects[object_index].vy)))>0.4))
                    {
                        return true;
                    }
                    
                    
                }
                else
                {
                    return false;
                    
                }

            }
            else
            {
                return false;
            }
            return false;

    }
v2x_s_cpm_t PublishCPM::add_objects_to_cpm(std::vector<in2lab_ros_msg::TrackObject> objects_to_publish,v2x_s_cpm_t s_cpm)
    {
        for(int i=0;i<objects_to_publish.size();i++)
        {
                s_cpm.as_perceived_objs[PublishCPM::no_of_objects].u8_obj_id=PublishCPM::no_of_objects;
                s_cpm.as_perceived_objs[PublishCPM::no_of_objects].u16_x_spd_conf=1;
                s_cpm.as_perceived_objs[PublishCPM::no_of_objects].u16_y_spd_conf=1;
                s_cpm.as_perceived_objs[PublishCPM::no_of_objects].b_sensor_id=true;
                s_cpm.as_perceived_objs[PublishCPM::no_of_objects].u8_sensor_id=CAMERA_RIGHT;
                
                
                if((objects_to_publish[i].obj_pose_x>132767 || objects_to_publish[i].obj_pose_x < -132768) || (objects_to_publish[i].obj_pose_y>132767 || objects_to_publish[i].obj_pose_x< -132768))
                {
                    s_cpm.as_perceived_objs[PublishCPM::no_of_objects].s32_x_dist_value=0;
                    s_cpm.as_perceived_objs[PublishCPM::no_of_objects].s32_y_dist_value=0;
                    s_cpm.as_perceived_objs[PublishCPM::no_of_objects].s32_z_dist_value=0;
                }

                else{
                    s_cpm.as_perceived_objs[PublishCPM::no_of_objects].s32_x_dist_value=objects_to_publish[i].obj_pose_x;
                    s_cpm.as_perceived_objs[PublishCPM::no_of_objects].s32_y_dist_value=objects_to_publish[i].obj_pose_y;
                    s_cpm.as_perceived_objs[PublishCPM::no_of_objects].s32_z_dist_value=objects_to_publish[i].obj_pose_z;
                }
                s_cpm.u32_perieved_obj_length=++PublishCPM::no_of_objects;
                fprintf(stderr, "Number of Objects=%u\n", s_cpm.u32_perieved_obj_length);
        }
        PublishCPM::no_of_objects=0;
        return s_cpm;
    }

void PublishCPM::cpm_publisher(v2x_s_session_t l_s_its ,v2x_s_pos_t *position, v2x_s_cpm_t s_cpm,v2x_s_gn_send_common_t s_send_data_shb)
{
    
    struct timespec s_now;
    int debug=0;
    v2x_u64_timestamp_t u64_now;
    v2x_e_err_t e_err = V2X_E_ERR_GENERAL;
    

    uint8_t buffer[1500UL] = {0};
    uint16_t length = 0U;

    
    //e_err=v2x_init_cpm(&s_cpm);
    clock_gettime(CLOCK_TAI, &s_now);
    u64_now = 1000u * s_now.tv_sec + s_now.tv_nsec / 1000000u + L_U64_TAI_OFFSET_BUG_MS;

    
    s_cpm.u16_generation_delta_time=u64_now;
    s_cpm.e_station_type=V2X_E_STATION_TYPE_ROADSIDE_UNIT;

    s_cpm.b_poc_segment_info=1;
    s_cpm.s_poc_segment_info.u8_this_segment=1;
    s_cpm.s_poc_segment_info.u8_total_segments=1;

    if(position->b_is_valid)
    {
        s_cpm.s_ref_pos.e_altitude_conf=V2X_E_CDD_ALTITUDE_CONF_UNAVAILABLE;
        s_cpm.s_ref_pos.s32_altitude_value=position->s32_altitude;
        s_cpm.s_ref_pos.s32_latitude=position->s32_latitude;
        s_cpm.s_ref_pos.s32_longitude=position->s32_longitude;
        s_cpm.s_ref_pos.u16_semi_major_conf=0;
        s_cpm.s_ref_pos.u16_semi_minor_conf=0;
        s_cpm.s_ref_pos.u16_semi_major_orientation=V2X_U16_HEADING_NA;
    }
    else

    {
        s_cpm.s_ref_pos.e_altitude_conf=V2X_E_CDD_ALTITUDE_CONF_UNAVAILABLE;
        s_cpm.s_ref_pos.s32_altitude_value=0;
        s_cpm.s_ref_pos.s32_latitude=0;
        s_cpm.s_ref_pos.s32_longitude=0;
        s_cpm.s_ref_pos.u16_semi_major_conf=0;
        s_cpm.s_ref_pos.u16_semi_minor_conf=0;
        s_cpm.s_ref_pos.u16_semi_major_orientation=V2X_U16_HEADING_NA;
    }

    if(camera_right_has_objects)
    {
        //Filter objects
        //Has the object already published before
        for(int i=0;i<camera_right_objects_list.size();i++)
        {
                if(is_new(camera_right_objects_list[i]))
                    {
                        objects_to_publish.push_back(camera_right_objects_list[i]);
                        continue;
                    }
                else if(is_moving(camera_right_objects_list[i]))
                    {
                        objects_to_publish.push_back(camera_right_objects_list[i]);
                        continue;
                    }
                else if(is_static(published_objects,camera_right_objects_list[i]))
                    {
                        objects_to_publish.push_back(camera_right_objects_list[i]);
                        continue;
                    } 
                                     

                
        }
        
    }

    if(camera_left_has_objects)
    {
        //Filter objects
        //Has the object already published before
        for(int i=0;i<camera_left_objects_list.size();i++)
        {
                if(is_new(camera_left_objects_list[i]))
                    {
                        objects_to_publish.push_back(camera_left_objects_list[i]);
                        continue;
                    }
                else if(is_moving(camera_left_objects_list[i]))
                    {
                        objects_to_publish.push_back(camera_left_objects_list[i]);
                        continue;
                    }
                else if(is_static(published_objects,camera_left_objects_list[i]))
                    {
                        objects_to_publish.push_back(camera_left_objects_list[i]);
                        continue;
                    }                                      

                
        }
        
    }

    if(camera_fisheye_has_objects)
    {
        //Filter objects
        //Has the object already published before
        for(int i=0;i<camera_fisheye_objects_list.size();i++)
        {
                if(is_new(camera_fisheye_objects_list[i]))
                    {
                        objects_to_publish.push_back(camera_fisheye_objects_list[i]);
                        continue;
                    }
                else if(is_moving(camera_fisheye_objects_list[i]))
                    {
                        objects_to_publish.push_back(camera_fisheye_objects_list[i]);
                        continue;
                    }
                else if(is_static(published_objects,camera_fisheye_objects_list[i]))
                    {
                        objects_to_publish.push_back(camera_fisheye_objects_list[i]);
                        continue;
                    }                                      

                
        }
        
    }

    if(radar_right_has_objects)
    {
        //Filter objects
        //Has the object already published before
        for(int i=0;i<radar_right_objects_list.size();i++)
        {
                if(is_new(radar_right_objects_list[i]))
                    {
                        objects_to_publish.push_back(radar_right_objects_list[i]);
                        continue;
                    }
                else if(is_moving(radar_right_objects_list[i]))
                    {
                        objects_to_publish.push_back(radar_right_objects_list[i]);
                        continue;
                    }
                else if(is_static(published_objects,radar_right_objects_list[i]))
                    {
                        objects_to_publish.push_back(radar_right_objects_list[i]);
                        continue;
                    }                                      

                
        }
        
    }

    if(radar_left_has_objects)
    {
        //Filter objects
        //Has the object already published before
        for(int i=0;i<radar_left_objects_list.size();i++)
        {
                if(is_new(radar_left_objects_list[i]))
                    {
                        objects_to_publish.push_back(radar_left_objects_list[i]);
                        continue;
                    }
                else if(is_moving(radar_left_objects_list[i]))
                    {
                        objects_to_publish.push_back(radar_left_objects_list[i]);
                        continue;
                    }
                else if(is_static(published_objects,radar_left_objects_list[i]))
                    {
                        objects_to_publish.push_back(radar_left_objects_list[i]);
                        continue;
                    }                                      

                
        }
        
    }

    if(lidar_left_has_objects)
    {
        //Filter objects
        //Has the object already published before
        for(int i=0;i<lidar_left_objects_list.size();i++)
        {
                if(is_new(lidar_left_objects_list[i]))
                    {
                        objects_to_publish.push_back(lidar_left_objects_list[i]);
                        continue;
                    }
                else if(is_moving(lidar_left_objects_list[i]))
                    {
                        objects_to_publish.push_back(lidar_left_objects_list[i]);
                        continue;
                    }
                else if(is_static(published_objects,lidar_left_objects_list[i]))
                    {
                        objects_to_publish.push_back(lidar_left_objects_list[i]);
                        continue;
                    }                                      

                
        }
        
    }

    if(lidar_right_has_objects)
    {
        //Filter objects
        //Has the object already published before
        for(int i=0;i<lidar_right_objects_list.size();i++)
        {
                if(is_new(lidar_right_objects_list[i]))
                    {
                        objects_to_publish.push_back(lidar_right_objects_list[i]);
                        continue;
                    }
                else if(is_moving(lidar_right_objects_list[i]))
                    {
                        objects_to_publish.push_back(lidar_right_objects_list[i]);
                        continue;
                    }
                else if(is_static(published_objects,lidar_right_objects_list[i]))
                    {
                        objects_to_publish.push_back(lidar_right_objects_list[i]);
                        continue;
                    }                                      

                
        }
        
    }






        //objects_to_publish into CPM Struct


                
        s_cpm=add_objects_to_cpm(objects_to_publish,s_cpm);

        /* ItsPduHeader */
        /* Byte 0: protocolVersion */
        buffer[length] = 2;
        length += 1;
        /* Byte 1: messageID - CPM */
        buffer[length] = 14U;
        length += 1;
        /* Byte 2 - 5: StationID */
        uint32_t station_id = htonl(13UL);
        memcpy(buffer + length, &station_id, sizeof(station_id));
        length += sizeof(station_id);

        length += v2x_encode_cpm(buffer + length, sizeof(buffer) - length, &s_cpm);
        e_err = v2x_gn_send_shb(&l_s_its, &s_send_data_shb, buffer, length);
        fprintf(stderr, "Number of Objects=%u\n", s_cpm.u32_perieved_obj_length);
        if(v2x_error(e_err)) 
        {
            fprintf(stderr, "Unable to send GeoNetworking message %s\n", v2x_get_err_str(e_err));
         
        } 
        else 
        {
                    printf("CPM Send. Successful. BTP-%s %u \n",
                   (V2X_E_BTP_A == s_send_data_shb.e_btp) ? "A" : (
                       (V2X_E_BTP_B == s_send_data_shb.e_btp) ? "B" : "Any"),
                   (unsigned short)s_send_data_shb.u16_dport);
        }

        //Append objects into Published Objects list
        //Published objects to cleared every 10second
        published_objects.insert(published_objects.end(),objects_to_publish.begin(),objects_to_publish.end());


        //Clear the contents of objects_to_publish
        objects_to_publish.clear();



        v2x_s_cpm_t cpm_d; //To Test Decode
        v2x_s_pdu_header_t s_pdu = V2X_S_PDU_INITIALIZER;

        if(debug)
        {
            v2x_decode_cpm_with_itspdu(buffer, length, &cpm_d,&s_pdu);

            
            fprintf(stderr, "Number of Objects=%u\n", cpm_d.u32_perieved_obj_length);
            fprintf(stderr, "Number of Sensors=%u\n", cpm_d.u32_sensor_info_length);
            fprintf(stderr, "Generated At=%u\n", cpm_d.u16_generation_delta_time);
            fprintf(stderr, "Latitude At=%d\n", cpm_d.s_ref_pos.s32_latitude);
            printf("Longitude At=%d\n", cpm_d.s_ref_pos.s32_longitude); 
            printf("Number of Sensors=%d\n",cpm_d.u32_sensor_info_length);


            std::cout<<"Total Objects to Publish:"<<objects_to_publish.size()<<"\n";
            std::cout<<"Total Objects to Publish:"<<objects_to_publish.size()<<"\n";
            std::cout<<"Total Objects Published:"<<published_objects.size()<<"\n";
            total_object_published_in_cpm=total_object_published_in_cpm+objects_to_publish.size();


        }

        //Clear CPM Object after publishing
            
        //Removing the Objects
            if(camera_right_has_objects)
            {
                camera_right_has_objects=false;               
                
            }
            if(camera_left_has_objects)
            {
                camera_left_has_objects=false;
            }
            
            if(camera_fisheye_has_objects)
            {
                camera_fisheye_has_objects=false;                
            }
            if(radar_right_has_objects)
            {
           
                radar_right_has_objects=false;
                
            }
            if(radar_left_has_objects)
            {

                radar_left_has_objects=false;
                
            }
            if(lidar_right_has_objects)
            {

                lidar_right_has_objects=false;   
                
            }
            if(lidar_left_has_objects)
            {

                lidar_left_has_objects=false;
                
            }
 
        PublishCPM::s_cpm={};
        camera_right_objects_list.clear();


        
    
    }
void PublishCPM::callBack_camera_right(const in2lab_ros_msg::TrackObjectList& msg)
    {               
        
        //Capture the Object Information
        if(msg.total_objects>0)
        {
            camera_right_has_objects=true;

            for(int i=0;i<msg.total_objects;i++)
                {
                    camera_right_objects_list.push_back(msg.object_list[i]);

                }
            
        } 
    }
void PublishCPM::callBack_camera_left(const in2lab_ros_msg::TrackObjectList& msg)
    {
        //
        //Capture the Object Information
        if(msg.total_objects>0)
        {

            camera_left_has_objects=true;

            for(int i=0;i<msg.total_objects;i++)
                {
                    camera_left_objects_list.push_back(msg.object_list[i]);

                }
            
        } 

    }
void PublishCPM::callBack_camera_fisheye(const in2lab_ros_msg::TrackObjectList& msg)
    {
    //Capture the Object Information

    if(msg.total_objects>0)
    {

        camera_fisheye_has_objects=true;

        for(int i=0;i<msg.total_objects;i++)
            {
                camera_fisheye_objects_list.push_back(msg.object_list[i]);

            }
        
    } 
    

    }
void PublishCPM::callBack_lidar_right(const in2lab_ros_msg::TrackObjectList& msg)
    {  
    if(msg.total_objects>0)
    {

        lidar_right_has_objects=true;

        for(int i=0;i<msg.total_objects;i++)
            {
                lidar_right_objects_list.push_back(msg.object_list[i]);

            }
        
    } 

    }

//Callback method to capture the objects from the left LIDAR sensor ros topic
void PublishCPM::callBack_lidar_left(const in2lab_ros_msg::TrackObjectList& msg)
    {
    if(msg.total_objects>0)
    {

        lidar_left_has_objects=true;

        for(int i=0;i<msg.total_objects;i++)
            {
                lidar_left_objects_list.push_back(msg.object_list[i]);

            }
        
    } 
    }

void PublishCPM::callBack_radar_right(const in2lab_ros_msg::TrackObjectList& msg)
    {
    if(msg.total_objects>0)
    {

        radar_right_has_objects=true;

        for(int i=0;i<msg.total_objects;i++)
            {
                radar_right_objects_list.push_back(msg.object_list[i]);

            }
        
    } 
    
    }

void PublishCPM::callBack_radar_left(const in2lab_ros_msg::TrackObjectList& msg)
    {
    if(msg.total_objects>0)
    {

        radar_left_has_objects=true;

        for(int i=0;i<msg.total_objects;i++)
            {
                radar_left_objects_list.push_back(msg.object_list[i]);

            }
        
    } 
    }

bool PublishCPM::camera_right_has_objects;
bool PublishCPM::camera_left_has_objects;
bool PublishCPM::camera_fisheye_has_objects;
bool PublishCPM::radar_right_has_objects;
bool PublishCPM::radar_left_has_objects;
bool PublishCPM::lidar_right_has_objects;
bool PublishCPM::lidar_left_has_objects;

bool PublishCPM::new_message_cam;//=false;
bool PublishCPM::new_message_cpm;

etsi_its_msgs::CAM PublishCPM::CAM_message;//=NULL;
etsi_its_msgs::CPM PublishCPM::CPM_message;//=NULL;

int PublishCPM::no_of_sensor;

v2x_s_cpm_t PublishCPM::s_cpm;
v2x_s_pos_t PublishCPM::pos;

}