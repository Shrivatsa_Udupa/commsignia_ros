# Commsignia ROS Node #

### Project IN2Lab ###
URL: [IN2Lab Project](http://in2lab.thi.de)

The R&D project IN²Lab aims to develop a system that enables testing of experimental autonomous driving functions on public roads with various challenges such as bus stops, bike lanes and roundabouts. Technically, the testing is achieved by a redundant source of sensor data from the roadside infrastructure and vehicle-to- infrastructure (V2X) communications. 

This repository is developed as a part of integration between sensor data from road side infrastructure and V2X Communication. The object detected from sensors hosted on Road side infrastructure were coverted into corresponding GeoNetworking (GN) messages and broadcasted with [Commsignia](https://www.commsignia.com) OB4 RSU.  **This repository can only be used with the C based SDK from Commsignia**. 

We achieve the following tasks with this codebase,
* Receive CPM and CAM message and publish them to corresponding ROS Topics 
* Convert detected objects from Sensors into CPM messages
* Filter the detected objects based on the CPM Generation Rules from ETSI TR 103 562 V2.1

The codebase was mainly developed for [IN2Lab Project](http://in2lab.thi.de)

* Developed for ROS1,Neuotic on Ubuntu 20.
* Tested with Commsignia RSU, SDK version "Unplugged-RT-y18.113.7-b69657-linuxm_obx_sdk_F-remote_c_sdk"

### How do I get set up? ###
* Clone the repository into your catkin workspace. 
* Copy the include and lib directory from SDK into the package. 
* Build the package with catkin_make
* Source the Workspace

### Configuration ###

Configure the name of the ROS topics on config.yaml in Config directory

IP Address of the Commsignia unit can be configured in launch file

### Who do I talk to? ###

* Shrivathsa Udupa <shrivatsa.udupa@carissma.eu>

